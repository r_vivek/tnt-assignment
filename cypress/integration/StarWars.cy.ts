/// <reference types="cypress" />

import ApiHandler from '../support/ApiHandler';
import StarWarsPage from '../support/page_objects/StarWarsPage';

describe('Star Wars Search web application', () => {
  beforeEach(() => {
    ApiHandler.initInterceptors();
    cy.visit('/');
  });
  it('Search for character (person) with a valid result', () => {
    StarWarsPage.searchUsingName('Luke Skywalker');
    StarWarsPage.arePersonResultsPresent();
    StarWarsPage.arePersonAttributesPresent();
  });
  it('Search for character (person) with an invalid result', () => {
    StarWarsPage.searchUsingName('Invalid');
    StarWarsPage.arePersonResultsNotPresent();
    StarWarsPage.checkNotFound('Not found.');
  });
  it('Search for planet with a valid result', () => {
    StarWarsPage.selectPlanetSearch();
    StarWarsPage.searchUsingName('Alderaan');
    StarWarsPage.arePlanetResultsPresent();
    StarWarsPage.arePlanetAttributesPresent();
  });
  it('Search for planet with an invalid result', () => {
    StarWarsPage.selectPlanetSearch();
    StarWarsPage.searchUsingName('Invalid');
    StarWarsPage.arePlanetResultsNotPresent();
    StarWarsPage.checkNotFound('Not found.');
  });
  it('Search for person with a valid result, clear the search and hit the Search button again and check for an empty result list', () => {
    StarWarsPage.searchUsingName('Luke Skywalker');
    StarWarsPage.arePersonResultsPresent();
    StarWarsPage.arePersonAttributesPresent();
    StarWarsPage.searchUsingName('');
    StarWarsPage.isPageEmpty();
  });
  it('Search for character (person) with a valid result using enter button', () => {
    StarWarsPage.searchUsingNameAndEnter('Luke Skywalker');
    StarWarsPage.arePersonResultsPresent();
    StarWarsPage.arePersonAttributesPresent();
  });
  it('When I search for a planet with valid result,switch to People and search again , I should get a “Not found” in the results.', () => {
    StarWarsPage.selectPlanetSearch();
    StarWarsPage.searchUsingName('Alderaan');
    StarWarsPage.arePlanetResultsPresent();
    StarWarsPage.arePlanetAttributesPresent();
    StarWarsPage.selectPeopleSearch();
    StarWarsPage.clickOnSearch();
    StarWarsPage.checkNotFound('Not found.');
  });
  it('Search for character (person) with more than one result', () => {
    StarWarsPage.searchUsingName('Darth');
    StarWarsPage.areResultsMoreThan(1);
  });
});
