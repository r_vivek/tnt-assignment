class ApiHandler {
  static initInterceptors() {
    cy.intercept({url: 'https://swapi.dev/api/people/*', method: 'GET'}).as('getPeople');
    cy.intercept({url: 'https://swapi.dev/api/planets/*', method: 'GET'}).as('getPlanets');
  }
}

export default ApiHandler;
