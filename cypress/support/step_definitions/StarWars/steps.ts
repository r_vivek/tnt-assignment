import {And, Before, Given, Then, When} from '@badeball/cypress-cucumber-preprocessor';
import ApiHandler from '../../ApiHandler';
import StarWarsPage from '../../page_objects/StarWarsPage';

Before(() => {
  ApiHandler.initInterceptors();
});
Given('The Star Wars application loads successfully', () => {
  cy.visit('/');
});
When('I search for {string},person', (name) => {
  StarWarsPage.searchUsingName(name);
});
And('it is a valid person', () => {
  StarWarsPage.arePersonResultsPresent();
});
Then('I should be able to see Gender,Birth year,Eye color and Skin color', () => {
  StarWarsPage.arePersonAttributesPresent();
});
And('it is not a valid person', () => {
  StarWarsPage.arePersonResultsNotPresent();
});
Then('I should be able to see {string} in the results', (searchString) => {
  StarWarsPage.checkNotFound(searchString);
});
When('I search for {string},planet', (name) => {
  StarWarsPage.selectPlanetSearch();
  StarWarsPage.searchUsingName(name);
});
And('it is a valid planet', () => {
  StarWarsPage.arePlanetResultsPresent();
});
Then('I should be able to see Population,Climate and Gravity', () => {
  StarWarsPage.arePlanetAttributesPresent();
});
And('it is not a valid planet', () => {
  StarWarsPage.arePlanetResultsNotPresent();
});
Then('I should be able to see an empty page', () => {
  StarWarsPage.isPageEmpty();
});
When('I search for {string},person using enter button', (name) => {
  StarWarsPage.searchUsingNameAndEnter(name);
});
And('I switch to people search', () => {
  StarWarsPage.selectPeopleSearch();
});
And('hit the search button', () => {
  StarWarsPage.clickOnSearch();
});
Then('I should be able to see more than {int} result', (count) => {
  StarWarsPage.areResultsMoreThan(count);
});
