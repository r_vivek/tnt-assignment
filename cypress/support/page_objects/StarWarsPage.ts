const nameField = '[id=query]';
const submitButton = '[data-qa=search-button]';
const characterCard = '[data-qa="character-card"]';
const personAttributes = new Map([
  ['genderLabel', '[data-qa="gender-label"]'],
  ['genderField', '[data-qa="gender"]'],
  ['birthYearLabel', '[data-qa="birth-year-label"]'],
  ['birthYearField', '[data-qa="birth-year"]'],
  ['eyeColorLabel', '[data-qa="eye-color-label"]'],
  ['eyeColorField', '[data-qa="eye-color"]'],
  ['skinColorLabel', '[data-qa="skin-color-label"]'],
  ['skinColorField', '[data-qa="skin-color"]'],
]);
const notFoundMessage = '[data-qa="not-found"]';
const planetCard = '[data-qa="planet-card"]';
const planetAttributes = new Map([
  ['populationLabel', '[data-qa="population-label"]'],
  ['populationField', '[data-qa="population"]'],
  ['climateLabel', '[data-qa="climate-label"]'],
  ['climateField', '[data-qa="climate"]'],
  ['gravityLabel', '[data-qa="gravity-label"]'],
  ['gravityField', '[data-qa="gravity"]'],
]);
const planetRadioButton = '[id="planets"]';
const peopleRadioButton = '[id="people"]';


class StarWarsPage {
  static searchUsingName(name) {
    // Search using the name
    cy.log('Searching using the name - ' + name).then(() => {
      if (name === '') {
        cy.get(nameField).clear();
      } else {
        cy.get(nameField).type(name);
      }
      cy.get(submitButton).click();
    });
  }

  static searchUsingNameAndEnter(name) {
    // Search using the name and hit the enter button
    cy.log('Searching using the name - ' + name).then(() => {
      if (name === '') {
        cy.get(nameField).clear();
      } else {
        cy.get(nameField).type(name + '{enter}');
      }
    });
  }

  static clickOnSearch() {
    // Click on search button
    cy.log('Clicking on search button').then(() => {
      cy.get(submitButton).click();
    });
  }

  static selectPlanetSearch() {
    // Choose planet search
    cy.log('Switching to planet search...').then(() => {
      cy.get(planetRadioButton).click();
    });
  }

  static selectPeopleSearch() {
    // Choose people search
    cy.log('Switching to people search...').then(() => {
      cy.get(peopleRadioButton).click();
    });
  }

  static arePersonResultsPresent() {
    // Wait for the response from the API and validate if there are search results
    cy.log('Verify if there is one or more search results').then(() => {
      cy.wait('@getPeople').then((xhr) => {
        expect(xhr.response.statusCode).to.be.eq(200);
        expect(xhr.response.body.count).to.be.greaterThan(0);
      });
      // Verify whether search results are present on the UI
      cy.get(characterCard).should('be.visible');
    });
  }

  static arePlanetResultsPresent() {
    // Wait for the response from the API and validate if there are search results
    cy.log('Verify if there is one or more search results').then(() => {
      cy.wait('@getPlanets').then((xhr) => {
        expect(xhr.response.statusCode).to.be.eq(200);
        expect(xhr.response.body.count).to.be.greaterThan(0);
      });
      // Verify whether search results are present on the UI
      cy.get(planetCard).should('be.visible');
    });
  }

  static arePersonAttributesPresent() {
    // Verify if the corresponding labels and fields of person are present
    cy.log('Verify if the corresponding labels and fields of person are present').then(() => {
      personAttributes.forEach((selector) => {
        cy.get(selector).should('be.visible');
      });
    });
  }

  static arePlanetAttributesPresent() {
    // Verify if the corresponding labels and fields of planet are present
    cy.log('Verify if the corresponding labels and fields of planet are present').then(() => {
      planetAttributes.forEach((selector) => {
        cy.get(selector).should('be.visible');
      });
    });
  }

  static arePersonResultsNotPresent() {
    // Wait for the response from the API and validate if there are no search results
    cy.log('Verify if there are no search results').then(() => {
      cy.wait('@getPeople').then((xhr) => {
        expect(xhr.response.statusCode).to.be.eq(200);
        expect(xhr.response.body.count).to.be.eq(0);
      });
      // Verify whether there are no search results on the UI
      cy.get(characterCard).should('not.exist');
    });
  }

  static arePlanetResultsNotPresent() {
    // Wait for the response from the API and validate if there are no search results
    cy.log('Verify if there are no search results').then(() => {
      cy.wait('@getPlanets').then((xhr) => {
        expect(xhr.response.statusCode).to.be.eq(200);
        expect(xhr.response.body.count).to.be.eq(0);
      });
      // Verify whether there are no search results on the UI
      cy.get(planetCard).should('not.exist');
    });
  }

  static checkNotFound(searchString) {
    // Check whether not found message is available
    cy.log('Check whether not found message is available').then(() => {
      cy.get(notFoundMessage).should('be.visible').should('have.text', searchString);
    });
  }

  static isPageEmpty() {
    // Check whether page is empty
    cy.log('Check whether page is empty').then(() => {
      cy.get(characterCard).should('not.exist');
      cy.get(planetCard).should('not.exist');
      cy.get(notFoundMessage).should('not.exist');
    });
  }

  static areResultsMoreThan(count) {
    // Wait for the response from the API and validate if there are more than {count} results
    cy.log('Verify if there are more than {count} results').then(() => {
      cy.wait('@getPeople').then((xhr) => {
        expect(xhr.response.statusCode).to.be.eq(200);
        expect(xhr.response.body.count).to.be.greaterThan(count);
      });
      // Verify whether search results are present on the UI
      cy.get(characterCard).should('have.length.above', count);
    });
  }
}

export default StarWarsPage;
