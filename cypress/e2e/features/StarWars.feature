Feature: Star Wars Search web application
  Scenario: Search for character (person) with a valid result
    Given The Star Wars application loads successfully
    When I search for "Luke Skywalker",person
    And it is a valid person
    Then I should be able to see Gender,Birth year,Eye color and Skin color

  Scenario: Search for character (person) with an invalid result
    Given The Star Wars application loads successfully
    When I search for "Invalid",person
    And it is not a valid person
    Then I should be able to see "Not found." in the results

  Scenario: Search for planet with a valid result
    Given The Star Wars application loads successfully
    When I search for "Alderaan",planet
    And it is a valid planet
    Then I should be able to see Population,Climate and Gravity

  Scenario: Search for planet with an invalid result
    Given The Star Wars application loads successfully
    When I search for "Invalid",planet
    And it is not a valid planet
    Then I should be able to see "Not found." in the results

  Scenario: Search for character (person) with a valid result, clear the “Search form” and hit the Search button again and check for an empty result list
    Given The Star Wars application loads successfully
    When I search for "Luke Skywalker",person
    And it is a valid person
    And I search for "",person
    Then I should be able to see an empty page

  Scenario: Search for character (person) with a valid result using enter button
    Given The Star Wars application loads successfully
    When I search for "Luke Skywalker",person using enter button
    And it is a valid person
    Then I should be able to see Gender,Birth year,Eye color and Skin color

  Scenario: When I searched for a planet and I got results, if I switch to People and search for the same thing , I should get a “Not found” in the results.
    Given The Star Wars application loads successfully
    When I search for "Alderaan",planet
    And it is a valid planet
    And I switch to people search
    And hit the search button
    Then I should be able to see "Not found." in the results

  Scenario: Search for character (person) with more than one result
    Given The Star Wars application loads successfully
    When I search for "Darth",person
    Then I should be able to see more than 1 result
